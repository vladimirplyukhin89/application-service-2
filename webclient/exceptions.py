from apilib.exceptions import BaseHTTPError
from restclient.exceptions import RestQueryError

__all__ = [
    'RestQueryError',
    'BaseHTTPError'
]


class AuthError(Exception):
    pass
