from logging import getLogger
from sanic import Sanic, Blueprint
from sanic.request import Request
from sanic.response import HTTPResponse, html

from .baseclasses import BaseJinjaView


LOGGER = getLogger('views')


def init(app: Sanic):
    view = Blueprint('view', url_prefix="/")

    view.add_route(Authorization.as_view(), '/')

    if app is not None:
        app.blueprint(view)

    return app


class Authorization(BaseJinjaView):
    logger = LOGGER

    async def get(self, request: Request) -> HTTPResponse:
        if request.ctx.session.get('jwt'):
            return html(await self.template(request, 'admin-panel.html'))
        return html(await self.template(request, 'authorization.html'))


async def abort(request: Request, exception: BaseException):
    return html(await request.app.templates.get_template("index.html").render_async(error=str(exception)))
