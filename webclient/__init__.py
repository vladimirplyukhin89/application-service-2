from logging.config import dictConfig
from sanic.request import Request
from sentry_sdk import init as sentry_init
from sentry_sdk.integrations.logging import LoggingIntegration
from sentry_sdk.integrations.redis import RedisIntegration
from sentry_sdk_extra.httpx import HttpxIntegration
from sentry_sdk_extra.sanic import SanicIntegration

from config.json import Config
from sanic_abc import App
from sanic_redis_session import SessionRedis
from sanic_templates_jinja2 import SanicJinjaTemplates

from . import middlewares, views, v1
from .constants import DEFAULT_STATIC_FOLDER, DEFAULT_TEMPLATES_FOLDER


__version__ = '1.1.0'


def create_app(config: Config = None) -> App:
    config = config if config is not None else Config()

    if config.get('sentry') and config.sentry.get('enable', False):
        sentry = config.extract('sentry')
        sentry_init(
            dsn=sentry.url,
            integrations=[
                SanicIntegration(),
                HttpxIntegration(),
                RedisIntegration(),
                LoggingIntegration()
            ],
            traces_sample_rate=sentry.get('rate', 1.0),
            environment=sentry.get('environment', 'develop'),
            release=__version__
        )

    if config.get('logging') is not None:
        dictConfig(config.get('logging'))

    app = App(
        name=config.extract('app').get('name', __name__),
        log_config=config.get('logging', {})
    )
    app.config.update_config(config.extract('app', uppercase=True))
    app.static('/static', config.app.get('static', DEFAULT_STATIC_FOLDER))
    app.exception(BaseException)(error_response)

    SessionRedis(**config.extract('session').all()).init_app(app, secure=False)
    SanicJinjaTemplates(templates=config.app.get('templates', DEFAULT_TEMPLATES_FOLDER), **config.get('jinja', {})).init_app(
        app,
        sentry={
            'version': __version__,
            'environment': config.get('sentry', {}).get('environment', 'develop'),
            'rate': config.get('sentry', {}).get('rate', 1.0)
        }
    )

    middlewares.init(app)
    v1.init(app)
    views.init(app)

    return app


async def error_response(request: Request, exception: BaseException):
    if request.path.startswith("/v1"):
        return await v1.error_response(request, exception)
    return await views.error_response(request, exception)
