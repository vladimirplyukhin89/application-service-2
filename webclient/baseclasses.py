from __future__ import annotations
import typing as t

from abc import abstractmethod
from logging import Logger, getLogger
from types import SimpleNamespace
from sanic.views import HTTPMethodView
from sanic.request import Request

from config.json import Config
from apilib import App
from restclient import AsyncRestClient
from restclient.auth import Auth, TokenAuth

from .exceptions import RestQueryError


class LoggerMixin:
    logger: Logger = getLogger()

    def __new__(cls, *_, **__):
        obj = super().__new__(cls)
        obj.logger = cls.logger.getChild(f'{cls.__name__}')
        return obj


class BaseRestClientMixin:
    config = Config(section='api')
    version = 'v1'

    async def proxy(self, request: Request, endpoint, *args, **kwargs):
        if 'auth' not in kwargs and (auth := self.auth(request)) is not None:
            kwargs['auth'] = auth

        headers = self.bypass_headers(request)
        if 'headers' in kwargs:
            headers.update(kwargs.pop('headers'))

        result = await self.call(request, endpoint, *args, headers=headers, **kwargs)
        if isinstance(result, dict) and result.get('auth'):
            request.ctx.session['jwt'] = result.pop('auth', {})['jwt']

        return result

    @property
    @abstractmethod
    def rest(self) -> AsyncRestClient:
        raise NotImplementedError()

    async def call(self, request: Request, endpoint, *args, **kwargs):
        try:
            result = await self.rest(request.method, endpoint, *args, **kwargs)
        except RestQueryError as ex:
            if ex.code != 401:
                raise ex
            if (jwt := await self.refresh_token(request.ctx)) is None:
                raise ex
            kwargs['auth'] = self.auth(request, jwt)
            result = await self.rest(request.method, endpoint, *args, **kwargs)
        return result

    async def refresh_token(self, ctx: SimpleNamespace) -> t.Optional[str]:
        if ctx.session.get('jwt') is None:
            return None

        try:
            result = await self.rest('put', 'token', json={'jwt': ctx.session['jwt']})
        except (RestQueryError, KeyError):
            return None

        if isinstance(result, dict) and result.get('jwt'):
            ctx.session['jwt'] = result['jwt']
        return ctx.session.get('jwt')

    @staticmethod
    def bypass_headers(request: Request) -> dict:
        result = {}

        if (rid := getattr(request.ctx, 'request_id', None)):
            result['x-request-id'] = rid

        return result

    @staticmethod
    def auth(request: Request, token: str = None) -> t.Optional[Auth]:
        token = token if isinstance(token, str) else request.ctx.session.get('jwt')
        return TokenAuth(token) if token else None


class BaseJinjaView(HTTPMethodView, LoggerMixin):

    @staticmethod
    async def template(request: Request, name: str, context: t.Dict[str, t.Any] = None) -> str:
        template = t.cast(App, request.app).extensions['jinja2'].get_template(name)
        return await template.render_async(**update_request_context(request, context))


class BaseRESTView(HTTPMethodView, LoggerMixin):

    @staticmethod
    def parse_args(request: Request) -> dict:
        return {k: v[0] if isinstance(v, list) and len(v) == 1 else v for k, v in request.args.items()}


def update_request_context(request: Request, context: t.Optional[t.Dict[str, t.Any]] = None) -> dict:
    context = context if context is not None else {}

    if request:
        context.setdefault("request", request)

    return context
