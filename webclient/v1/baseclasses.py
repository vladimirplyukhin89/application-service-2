from sanic.response import HTTPResponse

from restclient import AsyncRestClient

from ..baseclasses import BaseRESTView, BaseRestClientMixin
from .constants import (
    REST_API_ENDPOINTS,
    REST_DEFAULT_HEADERS
)


class RestClientMixin(BaseRestClientMixin):
    version = 'v1'

    @property
    def rest(self):
        return AsyncRestClient(
            address=f"{self.config.url}/{self.version}",
            endpoints=REST_API_ENDPOINTS,
            headers=REST_DEFAULT_HEADERS,
            timeout=None
        )

    @staticmethod
    def copy_headers(response: HTTPResponse) -> dict:
        return {'Content-Disposition': response.headers.get('Content-Disposition')}


class BaseREST(BaseRESTView, RestClientMixin):
    pass
