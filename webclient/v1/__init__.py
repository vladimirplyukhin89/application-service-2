from logging import getLogger
from sanic.request import Request
from sanic.response import HTTPResponse, json, raw

from sanic_abc import App, Api
from apilib.output import abort

from .baseclasses import BaseREST
from ..exceptions import RestQueryError, BaseHTTPError

VERSION = "v1"
LOGGER = getLogger(f"api.{VERSION}")


def init(app: App = None) -> Api:
    api = Api(f'api.{VERSION}', url_prefix=VERSION)

    api.new_routes({
        '/operator/': OperatorsResource,
        '/ticket/': TicketResource,
        '/address/<account:str>/': AddressResource,
        '/dates/<account:str>/': DatesResource,
    })

    return api.init_app(app)


class OperatorsResource(BaseREST):

    async def get(self, request: Request) -> HTTPResponse:
        return json(await self.proxy(request, 'operator'))


class TicketResource(BaseREST):

    async def post(self, request: Request) -> HTTPResponse:
        return json(await self.proxy(request, 'ticket', json=request.json))

class AddressResource(BaseREST):

    async def get(self, request: Request, account: str) -> HTTPResponse:
        return json(await self.proxy(request, 'address', account))


class DatesResource(BaseREST):

    async def get(self, request: Request, account: str) -> HTTPResponse:
        return json(await self.proxy(request, 'dates', account))


async def error_response(request: Request, exception: BaseException):
    name = f"abort.{request.ctx.request_id}" if hasattr(request.ctx, 'request_id') else "abort"
    description = None

    LOGGER.getChild(name).info(f"Abort: {type(exception)} {exception}")

    if isinstance(exception, RestQueryError):
        if exception.code == 401:
            request.ctx.session.pop('jwt', None)
        description = exception.message['description'] if isinstance(exception.message, dict) else description
        exception = BaseHTTPError(
            message=exception.message['message'] if isinstance(exception.message, dict) else exception.message,
            code=exception.code
        )

    return abort(exception, description)
