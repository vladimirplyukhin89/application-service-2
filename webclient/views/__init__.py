from __future__ import annotations
import typing as t

from logging import getLogger
from sanic.request import Request
from sanic.response import HTTPResponse, html

from sanic_abc import App, Views

from ..exceptions import RestQueryError, AuthError
from .baseclasses import BaseJinja


LOGGER = getLogger('views')


def init(app: App) -> Views:
    view = Views('view', url_prefix="/")

    view.new_routes({
        '/': Authorization,
        '/logout/': Logout
    })

    return view.init_app(app)


class Authorization(BaseJinja):
    logger = LOGGER

    async def get(self, request: Request) -> HTTPResponse:
        return html(await self.template(request, 'application-service.html'))

    async def post(self, request: Request) -> HTTPResponse:
        try:
            response = await self.proxy(request, 'token', json=self.extract_form_data(request.form))
        except RestQueryError as ex:
            ex = t.cast(RestQueryError, ex)
            raise AuthError(ex.message['message'] if isinstance(ex.message, dict) else ex.message) from ex

        if not response.get('jwt'):
            raise AuthError('Неизвестная ошибка')

        request.ctx.session['jwt'] = response['jwt']
        return html(await self.template(request, 'application-service.html'))

    @ staticmethod
    def extract_form_data(form: dict) -> dict:
        result = {}

        for field in ('login', 'password'):
            if form.get(field, None):
                result[field] = "".join(form[field])

        return result


class Logout(BaseJinja):
    logger = LOGGER

    async def post(self, request: Request) -> HTTPResponse:
        request.ctx.session.pop('jwt', None)
        return html(await self.template(request, 'application-service.html'))


async def error_response(request: Request, exception: BaseException):
    return html(await BaseJinja.template(request, "application-service.html", context={'error': str(exception)}))
