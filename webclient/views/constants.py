from types import MappingProxyType

from ..constants import REST_DEFAULT_HEADERS

__all__ = [
    'REST_DEFAULT_HEADERS'
]

REST_API_ENDPOINTS = MappingProxyType({
    "token": "token"
})
