from sanic.request import Request

from restclient import AsyncRestClient

from ..baseclasses import BaseJinjaView, BaseRestClientMixin
from .constants import (
    REST_API_ENDPOINTS,
    REST_DEFAULT_HEADERS
)


class RestClientMixin(BaseRestClientMixin):
    version = 'v1'

    @property
    def rest(self):
        return AsyncRestClient(
            address=f"{self.config.url}/{self.version}",
            endpoints=REST_API_ENDPOINTS,
            headers=REST_DEFAULT_HEADERS,
            timeout=None
        )

    @staticmethod
    def parse_args(request: Request) -> dict:
        return {k: v[0] if isinstance(v, list) and len(v) == 1 else v for k, v in request.args.items()}


class BaseJinja(BaseJinjaView, RestClientMixin):
    pass
