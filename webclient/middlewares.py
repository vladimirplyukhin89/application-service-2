from logging import getLogger, Logger

from apilib import App
from apilib.middlewares import log_request, log_response, set_request_id, add_request_id


def init(app: App, logger: Logger = None) -> App:
    logger = logger if isinstance(logger, Logger) else getLogger('http')

    app.request_middleware.append(add_request_id())
    app.request_middleware.append(log_request(logger.getChild('request')))

    app.response_middleware.appendleft(set_request_id())
    app.response_middleware.append(log_response(logger.getChild('response')))

    return app
