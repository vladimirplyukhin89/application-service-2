from types import MappingProxyType

REST_DEFAULT_HEADERS = MappingProxyType({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
})


DEFAULT_TEMPLATES_FOLDER = "./templates"
DEFAULT_STATIC_FOLDER = "/app/htdocs"
