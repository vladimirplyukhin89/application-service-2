ARG env=production
ARG package_target=/app/dist

ARG static=registry.itpc.ru/docker/develop/typescript/app:4.2
ARG templates=registry.itpc.ru/docker/os/alpine:3.15
ARG compile=registry.itpc.ru/docker/compile/python:5
ARG runtime=registry.itpc.ru/docker/runtime/python/app:9


FROM ${static} as static

ARG env
ARG package_target
ARG package_tmp=/usr/local/src/app

ARG FONTAWESOME_NPM_AUTH_TOKEN

ENV ENVIRONMENT=${env} \
    GULP_OUTPUT=${package_tmp}/static

USER root
RUN set -xe \
    && mkdir -p \
    ${package_tmp} \
    ${package_target} \
    && chown -R ${APP_USER} \
    ${package_tmp} \
    ${package_target}

COPY --chown=${APP_USER} ./.source ${package_tmp}

USER ${APP_USER}
RUN set -xe \
    && task -d ${package_tmp} \
    && tar -czf static.tar.gz -C ${package_tmp}/static . \
    && mv static.tar.gz ${package_target} \
    && rm -rf ${package_tmp}/*


FROM ${templates} as templates

ARG package_target
ARG package_tmp=/usr/local/src/app

COPY ./templates ${package_tmp}/templates

RUN set -xe \
    && mkdir -p ${package_target} \
    && tar -czvf templates.tar.gz -C ${package_tmp} . \
    && mv templates.tar.gz ${package_target} \
    && rm -rf ${package_tmp}/*


FROM ${compile} as package

ARG env
ARG package_target

ENV PACKAGE_TARGET=${package_target} \
    ENVIRONMENT=${env}

COPY --chown=${APP_USER} . .

RUN set -xe \
    && task -v build \
    && rm -rf *


FROM ${runtime} as runtime

ARG env
ARG package_target
ARG package_tmp=/usr/local/src/app
ARG htdocs=/app/htdocs

ENV PACKAGE_TARGET=/app/project \
    ENVIRONMENT=${env}

COPY --from=package ${package_target} ${package_tmp}/package
COPY --from=templates ${package_target} ${package_tmp}/templates
COPY --from=static ${package_target} ${package_tmp}/static

RUN set -xe \
    && pip install --no-deps ${package_tmp}/package/*.whl -t ${PACKAGE_TARGET} \
    && task -d $PACKAGE_TARGET deps-install \
    && tar -xzf ${package_tmp}/templates/*.tar.gz -C ${PACKAGE_TARGET} \
    && mkdir -p $htdocs \
    && tar -xzf ${package_tmp}/static/*.tar.gz -C $htdocs \
    && chown -R ${APP_USER} ${APP_PATH} \ 
    && rm -rf ${package_tmp}

USER ${APP_USER}
VOLUME /app/data
EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/task"]
CMD ["server"]
