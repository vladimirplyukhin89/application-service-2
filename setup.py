#!/usr/bin/env python
from tomlkit import loads as toml_loads
from yaml import safe_load as yaml_loads

from app_cli import Cli, Command, Argument, echo


def create_cli() -> Cli:

    cli = Cli(__file__)
    cli.add_command('extra-index-patch', extra_index())
    cli.add_command('get-version', get_version())

    return cli


def extra_index() -> Command:

    def _patch(filename: str = None):
        filename = filename if filename is not None else "requirements.txt"

        with open("pyproject.toml", encoding="utf-8") as prj, open(filename, "a") as rqst:
            project = dict(toml_loads(prj.read()))
            for eir in [s['url'] for s in project['tool']['poetry']['source'] if not s.get('default')]:  # type: ignore
                rqst.write(f"--extra-index-url {eir}\n")

    return Command(
        name='extra-index',
        callback=_patch,
        params=[
            Argument(['filename'], required=False)
        ],
        help="Patch requirements.txt by extra indexes (strange poetry case)"
    )


def get_version() -> Command:

    def _version():
        with open(".config/meta.yml", encoding="utf-8") as meta:
            project = yaml_loads(meta)
            echo(project['version'])

    return Command(
        name='get-version',
        callback=_version,
        help="Print current version of application"
    )


if __name__ == '__main__':
    create_cli().start()
