from __future__ import annotations
import typing as t

from pprint import pformat
from configparser import ConfigParser

from app_cli import Cli, Command, Option, echo

import webclient as service


def create_cli(app: service.App = None) -> Cli:
    app = app if isinstance(app, service.App) else service.create_app()

    cli = Cli(service=service, app=app)
    cli.add_command('shell', shell(**cli.context))
    cli.add_subcommand('run', run(app), help='Run application parts')
    cli.add_subcommand('debug', debug(app), help='Run debugger for parts')
    cli.add_subcommand('show', show(app), help='Display information about application')
    if (subcommands := getattr(service, 'create_extra_commands', None)) is not None:
        subcommands(cli)

    return cli


def shell(**context) -> Command:
    try:
        from IPython import start_ipython as ipython_shell
        from IPython.terminal.ipapp import load_default_config as ipython
    except ImportError:
        from code import interact as python_shell

    def _shell():
        return ipython_shell(argv=[], config=ipython(), user_ns=context) if 'ipython' in locals() else python_shell(local=context)

    return Command(
        name='shell',
        context_settings={'ignore_unknown_options': True},
        callback=_shell,
        help="Python interpreter"
    )


def run(app: service.App) -> t.Dict[str, Command]:
    def server():
        echo(f"[{app.name}] Start server in production mode")
        return app.run(
            host=app.config['HTTPSERVER'].get('host', '0.0.0.0'),
            port=app.config['HTTPSERVER'].get('port', 8080),
            debug=app.config.get('DEBUG', False)
        )

    return {
        'server': Command(name='server', callback=server, help='Start Web Server')
    }


def debug(app: service.App) -> t.Dict[str, Command]:
    wait_option = Option(param_decls=["--wait"], is_flag=True, help="Wait debugger before start")

    def server(wait: bool = False):
        echo(f"[{app.name}] Start server in debug mode (Waiting: {wait})")
        _enable_debug(wait=wait)
        return app.run(
            host=app.config['HTTPSERVER'].get('host', '0.0.0.0'),
            port=app.config['HTTPSERVER'].get('port', 8080),
            debug=True,
            auto_reload=False
        )

    def _enable_debug(wait: bool = False):
        try:
            import ptvsd
        except ImportError:
            return echo('Need ptvsd package')

        ptvsd.enable_attach()
        if wait:
            ptvsd.wait_for_attach()

    return {
        'server': Command(name='server', callback=server, params=[wait_option], help='Start Web Server')
    }


def show(app: service.App) -> t.Dict[str, Command]:
    def config():
        '''Application config'''
        _table_print('App Config')
        echo(pformat(app.config))

    def description():
        '''Base information'''
        _table_print(service.__name__)
        settings = ConfigParser()
        settings.read('setup.cfg')
        echo(f"Version: {settings['metadata'].get('version', 'unknown')}\n"
             f"Description:\n{settings['metadata'].get('description', 'unknown')}\n")

    def routes():
        '''Print routes information'''
        string_size = 90
        headers = ("Uri", "Name")
        echo(f"{headers[0]}{(' ' * (string_size // 2 - len(headers[0])))}{headers[1]}")
        echo(''.ljust(string_size, '-'))
        for route in app.router.routes_all.values():
            line = (route.uri, route.name)
            echo(f"{line[0]}{(' ' * (string_size // 2 - len(line[0])))}{line[1]}")

    def _table_print(*headers):
        out = '    '.join(headers)
        echo(out)
        echo('-' * len(out))

    return {
        'config': Command(name='config', callback=config, help='Prints application config'),
        'description': Command(name='description', callback=description, help='Base information'),
        'routes': Command(name='routes', callback=routes, help='Print http routes')
    }


if __name__ == '__main__':
    create_cli().start()
