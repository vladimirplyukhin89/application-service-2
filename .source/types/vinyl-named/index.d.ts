// Type definitions for vinyl-named 2.0
// Project: https://github.com/hparra/gulp-rename
// Definitions by: Asana <https://asana.com>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference types="node"/>

interface named {
  (): NodeJS.ReadWriteStream,
  (obj: string | namedFn): NodeJS.ReadWriteStream
}

interface namedFn {
  (file: string): string
}

declare var named: named
export = named;


