# Installation
> `npm install --save @types/gulp-rename`

# Summary
This package contains type definitions for gulp-rename (https://github.com/hparra/gulp-rename).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/gulp-rename.

### Additional Details
 * Last updated: Mon, 23 Nov 2020 21:40:14 GMT
 * Dependencies: [@types/vinyl](https://npmjs.com/package/@types/vinyl), [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Asana](https://asana.com).
