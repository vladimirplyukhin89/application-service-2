// Type definitions for gulp-concat-css 2.0
// Project: https://github.com/hparra/gulp-rename
// Definitions by: Asana <https://asana.com>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/// <reference types="node"/>

interface concatCss {
  (destFile: string): NodeJS.ReadWriteStream
  (destFile: string, options?: concatCss.Options): NodeJS.ReadWriteStream
}

interface Options {
  inlineImports?: boolean
  rebaseUrls?: boolean
  includePaths?: string[]
  commonBase?: string
}

declare var concatCss: concatCss;
export = concatCss;
