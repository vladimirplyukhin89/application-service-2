import GulpClient, * as gulp from "gulp"
import path from "path"

import del from "del"
import minify from "gulp-minify"
import webpack from "webpack-stream"
import named from "vinyl-named"

import { SOURCE, PATH } from "../constants"

const clean = () => del(path.join(PATH.js, "/*"), { force: true })
const app = () =>
  gulp
    .src([path.join(SOURCE.app, "js/app-application-service/index.tsx")])
    .pipe(named())
    .pipe(
      webpack({
        mode: "development",
        entry: {
          application: "app-application-service/index.tsx",
        },
        output: { filename: "[name].js" },
        resolve: {
          modules: [path.resolve(SOURCE.app, "js"), "node_modules"],
          extensions: [".ts", ".tsx", ".js", ".json"],
        },
        optimization: {
          splitChunks: {
            cacheGroups: {
              vendor: {
                test: /node_modules/,
                chunks: "initial",
                name: "vendor",
                enforce: true,
              },
            },
          },
        },
        module: {
          rules: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { enforce: "pre", test: /.js$/, loader: "source-map-loader" },
          ],
        },
      })
    )
    .pipe(
      minify({
        ext: { min: ".js" },
        noSource: true,
      })
    )
    .pipe(gulp.dest(PATH.js))

export const js = gulp.series(clean, app)
gulp.task("js.clean", clean)
gulp.task("js.app", app)
