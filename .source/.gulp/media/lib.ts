import * as gulp from "gulp"
import path from "path"

import flatten from "gulp-flatten"

import { SOURCE, PATH } from "../constants"

export const fontAwesome = () =>
  gulp
    .src([path.join(SOURCE.yarn, "@fortawesome/fontawesome-free/webfonts/*.*")])
    .pipe(flatten())
    .pipe(gulp.dest(path.join(PATH.media, "fonts")))
