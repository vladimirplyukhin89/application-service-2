import * as gulp from 'gulp'
import path from 'path'
import del from 'del'

import * as lib from "./lib"
import * as app from "./app"
import { PATH } from '../constants'

const clean = () => del(path.join(PATH.media, '/*'), { force: true })

const mediaLib = gulp.series(lib.fontAwesome)
const mediaApp = gulp.parallel(app.media)

gulp.task('media.clean', clean)
gulp.task('media.lib', mediaLib)
gulp.task('media.app', mediaApp)
export const media = gulp.series(clean, mediaLib, mediaApp)
