import * as gulp from 'gulp'
import path from 'path'

import { PATH, SOURCE } from '../constants'

export const media = () => gulp.src([path.join(SOURCE.app, 'media/**/*.*')])
  .pipe(gulp.dest(PATH.media))
