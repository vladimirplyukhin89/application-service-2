import path from 'path'

export const
  TARGET = process.env.GULP_OUTPUT ? process.env.GULP_OUTPUT : './dist',
  SOURCE = {
    app: "./src",
    yarn: "./node_modules"
  },
  PATH = {
    root: TARGET,
    js: path.join(TARGET, 'js'),
    css: path.join(TARGET, 'css'),
    media: path.join(TARGET, 'media')
  }