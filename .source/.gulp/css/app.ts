import * as gulp from 'gulp'
import path from 'path'

import flatten from 'gulp-flatten'
import sass from 'gulp-sass'
import csso from 'gulp-csso'

import { PATH, SOURCE } from './constants'

export const
  css = () => gulp.src([path.join(SOURCE.app, 'css/*.css')])
    .pipe(flatten())
    .pipe(csso())
    .pipe(gulp.dest(PATH.css)),

  scss = () => gulp.src([path.join(SOURCE.app, 'css/*.scss')])
    .pipe(sass())
    .pipe(flatten())
    .pipe(csso())
    .pipe(gulp.dest(PATH.css))
