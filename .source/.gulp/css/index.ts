import GulpClient, * as gulp from "gulp"
import path from "path"

import del from "del"

import { PATH } from "./constants"
import * as lib from "./lib"
import * as app from "./app"

const clean = () => del(path.join(PATH.css, "/*"), { force: true })

const cssLib = gulp.series(
  lib.create,
  gulp.parallel(lib.loadAwesome, lib.fontAwesome, lib.bootstrap),
  lib.vendor,
  lib.clean
)
const cssApp = gulp.parallel(app.css, app.scss)

export const css = gulp.series(clean, cssLib, cssApp)
gulp.task("css.clean", clean)
gulp.task("css.lib", cssLib)
gulp.task("css.app", cssApp)
gulp.task("css.app.css", app.css)
gulp.task("css.app.scss", app.scss)

/**
 * 1. task modules types
 * 2. gulp
 */
