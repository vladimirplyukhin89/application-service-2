import * as gulp from "gulp"
import path from "path"

import flatten from "gulp-flatten"
import concat from "gulp-concat-css"
import replace from "gulp-replace"
import rename from "gulp-rename"
import csso from "gulp-csso"
import del from "del"

import { SOURCE, PATH, TEMPDIR } from "./constants"

export const loadAwesome = () =>
    gulp
      .src([
        path.join(SOURCE.yarn, "load-awesome/css/ball-clip-rotate.css"),
        path.join(SOURCE.yarn, "load-awesome/css/ball-beat.css"),
      ])
      .pipe(flatten())
      .pipe(concat("load-awesome.css"))
      .pipe(gulp.dest(TEMPDIR)),
  fontAwesome = () =>
    gulp
      .src([
        path.join(SOURCE.yarn, "@fortawesome/fontawesome-free/css/all.css"),
      ])
      .pipe(flatten())
      .pipe(replace("../webfonts", "/static/media/fonts"))
      .pipe(rename("font-awesome.css"))
      .pipe(gulp.dest(TEMPDIR)),
  bootstrap = () =>
    gulp
      .src([path.join(SOURCE.yarn, "bootstrap/dist/css/bootstrap.css")])
      .pipe(flatten())
      .pipe(replace("../fonts", "../media"))
      .pipe(gulp.dest(TEMPDIR)),
  vendor = () =>
    gulp
      .src([path.join(TEMPDIR, "*.css")])
      .pipe(concat("vendor.css"))
      .pipe(csso())
      .pipe(gulp.dest(PATH.css)),
  clean = () => del(path.join(TEMPDIR), { force: true }),
  create = () => gulp.src("*.*", { read: false }).pipe(gulp.dest(TEMPDIR))
