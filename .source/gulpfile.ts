/* eslint-disable no-console */
import * as gulp from "gulp"
import path from "path"

import { TARGET, SOURCE, css, js, media } from "./.gulp"

export * from "./.gulp"

console.log(`Target: ${TARGET}`)
process.on("unhandledRejection", (reason) =>
  console.log("Unhandled Rejection:\n", reason)
)

gulp.task("default", gulp.parallel(css, js, media))
gulp.task(
  "clean",
  gulp.parallel(
    gulp.task("css.clean"),
    gulp.task("js.clean"),
    gulp.task("media.clean")
  )
)

export const watch = (): void => {
  gulp.watch(
    [
      path.join(SOURCE.app, "js/**/*.tsx"),
      path.join(SOURCE.app, "js/**/*.tsx"),
    ],
    gulp.task("js.app")
  )
  gulp.watch([path.join(SOURCE.app, "css/**/*.css")], gulp.task("css.app.css"))
  gulp.watch(
    [path.join(SOURCE.app, "css/**/*.scss")],
    gulp.task("css.app.scss")
  )
  gulp.watch(path.join(SOURCE.app, "media/*.*"), gulp.task("media.app"))
}
