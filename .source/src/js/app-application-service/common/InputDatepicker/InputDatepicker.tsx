import React, { forwardRef } from "react"
import { InputDatepickerProps } from "./../../models"

const InputDatepicker: React.FC<InputDatepickerProps> = (
  { value, onClick, onChange },
  ref
) => {
  return (
    <input
      className="custom_input"
      placeholder="Дата поверки"
      type="text"
      value={value}
      ref={ref}
      onChange={(e) => onChange(e.target.value)}
      onClick={onClick}
    />
  )
}

export default forwardRef(InputDatepicker)
