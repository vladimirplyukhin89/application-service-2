import React, { useContext } from "react"
import { Popup } from "itpc-ui-kit"

import PopupContext from "../../context/Context"

export const WarningPopup: React.FC = (props) => {
  const { openPopup, onHandlePopup } = useContext(PopupContext)

  return (
    <Popup
      isOpen={openPopup}
      onClose={onHandlePopup}
      title="Предупреждение"
      variant="error"
      position="bottom-right"
    >
      {props.children}
    </Popup>
  )
}
