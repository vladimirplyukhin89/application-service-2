import React, { useContext } from "react"
import { Button, Modal, ModalContent, ModalFooter } from "itpc-ui-kit"

import ModalContext from "../../context/Context"

export const SuccessModal: React.FC = () => {
  const { openModal, onHandleModal } = useContext(ModalContext)

  return (
    <Modal title="Отправка формы" isOpen={openModal} onClose={onHandleModal}>
      <ModalContent>Форма отправлена успешно!</ModalContent>
      <ModalFooter>
        <Button onPress={onHandleModal}>Ok</Button>
      </ModalFooter>
    </Modal>
  )
}
