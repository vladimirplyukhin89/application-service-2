import React from "react"
import { IAddress } from "./../../models"

export const Address: React.FC<IAddress> = ({ address }: IAddress) => {
  return <span className="address">Адрес: {address}</span>
}
