import React, { useState } from "react"

import { InputState, InputType, ValidationState } from "../types"

interface Props {
  id: string
  name: string
  type?: InputType
  value?: string
  disabled?: boolean
  maxLength?: number
  placeholder?: string
  state?: InputState
  validationState?: ValidationState
  errorMessage?: string
  className?: string
  isShowButton?: boolean
  buttonChild?: React.ReactNode
  onBlur?: () => void
  onFocus?: () => void
  onChange?: (value: string) => void
  onClickButton?: (e: React.MouseEvent<HTMLButtonElement>) => void
}

export const TextField: React.FC<Props> = ({
  id = "itpc-text-field",
  name = "itpc-text-field",
  type = "text",
  value = "",
  disabled = false,
  maxLength,
  placeholder = "",
  state = "default",
  validationState = "valid",
  errorMessage = "",
  className,
  isShowButton = false,
  buttonChild,
  onBlur,
  onFocus,
  onChange,
  onClickButton,
}) => {
  const [focused, onHandleFocused] = useState<boolean>(false)

  const onBlurInput = (): void => {
    onHandleFocused(false)

    if (onBlur) {
      onBlur()
    }
  }

  const onFocusInput = (): void => {
    onHandleFocused(true)

    if (onFocus) {
      onFocus()
    }
  }

  const onChangeInput = (event: React.ChangeEvent<HTMLInputElement>): void => {
    if (onChange) {
      onChange(event.currentTarget.value)
    }
  }

  return (
    <div
      className={`itpc-field-wrap ${
        validationState === "invalid" && "itpc-field-wrap_error"
      } ${className}`}
    >
      <div className="itpc-input-wrap">
        <label
          htmlFor={id}
          className={`itpc-input-label ${
            disabled && "itpc-input-label_disabled"
          } ${(focused || value.length) && "itpc-input-label_focused"} ${
            validationState === "invalid" && "itpc-input-label_error"
          }`}
        >
          {placeholder}
        </label>
        <input
          id={id}
          name={name}
          type={type}
          value={value}
          disabled={disabled}
          maxLength={maxLength}
          onFocus={onFocusInput}
          onBlur={onBlurInput}
          onChange={onChangeInput}
          className={`itpc-input ${
            (focused || value.length) && "itpc-input_focused"
          } ${disabled && "itpc-input_disabled"}`}
        />
        {isShowButton && (
          <button
            type="button"
            className="itpc-input-wrap-button"
            onClick={onClickButton}
          >
            {buttonChild}
          </button>
        )}
      </div>

      {validationState === "invalid" && (
        <span className="itpc-field-error-message">{errorMessage}</span>
      )}
    </div>
  )
}
