export type InputType = "password" | "text"
export type InputState =
  | "default"
  | "cancel"
  | "loading"
  | "success"
  | "warning"
export type ValidationState = "valid" | "invalid"
