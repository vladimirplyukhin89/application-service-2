import React, { useEffect, useState } from "react"
import { Preloader } from "itpc-ui-kit"

import { TextField } from "../index"
import { useClickOutside } from "../../hooks/index"

interface SearchFieldProps {
  defaultItem?: string
  items: { id: string; username: string }[]
  placeholder?: string
  isLoading: boolean
  isClear?: boolean
  handleClear?(): void
  fetchData?(value: string): Promise<void>
  onChange(id: string): void
}

export const SearchField: React.FC<SearchFieldProps> = ({
  defaultItem,
  items,
  placeholder,
  isLoading,
  isClear = false,
  handleClear,
  fetchData,
  onChange,
}) => {
  const [isOpenedSuggestions, setIsOpenedSuggestions] = useState<boolean>(false)
  const [value, setValue] = useState<string>(
    defaultItem
      ? items.find((item) => item.id === defaultItem).username ?? ""
      : ""
  )
  const [currentItem, setCurrentItem] = useState<string>(defaultItem ?? "")

  const ref = useClickOutside<HTMLDivElement>(() =>
    setIsOpenedSuggestions(false)
  )

  const onChangeValue = (value: string): void => {
    setValue(value)
  }

  const onChangeItem = (id: string): void => {
    setIsOpenedSuggestions(false)
    setCurrentItem(id)
    setValue(items.find((item) => item.id === id).username ?? value)
    onChange(id)
  }

  const onClear = (): void => {
    setIsOpenedSuggestions(false)
    setValue("")
    setCurrentItem("")
    onChange("")
  }

  const filterItems = (item: { id: string; username: string }): boolean => {
    if (!value.length) {
      return true
    }

    return item?.username
      ?.toLocaleLowerCase()
      .includes(value.toLocaleLowerCase())
  }

  useEffect(() => {
    if (fetchData && value.length) {
      fetchData(value)
    }
  }, [value])

  useEffect(() => {
    if (isClear) {
      onClear()
      handleClear()
    }
  }, [isClear])

  const filteredItems = items.filter(filterItems)

  return (
    <div className="search-field_wrap" ref={ref}>
      <div className="search-field">
        <div className="search-field__input_wrap">
          <TextField
            id="search-field"
            name="search-field"
            placeholder="Оператор"
            onChange={onChangeValue}
            onFocus={() => setIsOpenedSuggestions(true)}
            value={value.trimStart()}
          />
        </div>
        {isLoading && (
          <div className="search-field__loading">
            <Preloader />
          </div>
        )}
        {!isLoading && Boolean(value.length) && (
          <button className="search-field__btn" onClick={onClear}>
            <i className="fa-solid fa-xmark" />
          </button>
        )}
      </div>
      {!!filteredItems.length && (
        <div
          className={`search-field__suggestions ${
            isOpenedSuggestions && "search-field__suggestions_opened"
          }`}
        >
          {filteredItems.map((item) => (
            <div
              key={item.id}
              id={item.id}
              className={`search-field__suggestion ${
                item.id === currentItem && "search-field__suggestion_active"
              }`}
              onClick={() => onChangeItem(item.id)}
            >
              {item.username}
            </div>
          ))}
        </div>
      )}
    </div>
  )
}
