export interface IDatepicker {
  selectedDate: Date | null
  setSelectedDate: React.Dispatch<React.SetStateAction<Date>>
  days: number[]
  children?: React.ReactNode
}
