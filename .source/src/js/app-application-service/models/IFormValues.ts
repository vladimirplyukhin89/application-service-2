export interface IFormValues {
  account: string
  firstName: string
  patronymicName?: string
  secondName: string
  firstMobile: string
  secondMobile?: string
  email?: string
  date: string
  operatorId: string
}
