export interface InputDatepickerProps {
  className?: string
  value: string
  onChange: (value: string) => void
  onClick: () => void
}
