export interface IFormErrors {
  accountError?: string
  firstNameError?: string
  patronymicNameError?: string
  secondNameError?: string
  firstMobileError?: string
  secondMobileError?: string
  emailError?: string
  dateError?: string
}
