export interface IError {
  status: number
  message: string
  description: string
}
