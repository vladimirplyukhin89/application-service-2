import React, { createContext, useState } from "react"

import { IContext } from "../models"

const Context = createContext(null)

export const ContextProvider: React.FC<IContext> = ({
  children,
}): React.ContextType<typeof Context> => {
  const [openPopup, setOpenPopup] = useState<boolean>(false)
  const [openModal, setOpenModal] = useState<boolean>(false)
  // Для вывода ошибок в попапе при запросе/отправке данных
  const [errorForm, setErrorForm] = useState<string>("")
  const [errorDate, setErrorDate] = useState<string>("")
  const [errorAddress, setErrorAddress] = useState<string>("")
  const [errorOperators, setErrorOperators] = useState<string>("")

  const onHandlePopup = (): void => {
    setOpenPopup(!openPopup)
  }

  const onHandleModal = (): void => {
    setOpenModal(!openModal)
  }

  return (
    <Context.Provider
      value={{
        openPopup,
        setOpenPopup,
        onHandlePopup,
        openModal,
        setOpenModal,
        onHandleModal,
        errorForm,
        setErrorForm,
        errorDate,
        setErrorDate,
        errorAddress,
        setErrorAddress,
        errorOperators,
        setErrorOperators,
      }}
    >
      {children}
    </Context.Provider>
  )
}

export default Context
