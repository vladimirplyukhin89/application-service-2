import React, { useState, useEffect, useContext, Dispatch } from "react"
import { useDispatch } from "react-redux"

import Context from "./../../context/Context"
import { SearchField } from "../../common"
import { getOperators } from "../../API"
import { getOperatorsUrl } from "../../constants/api"
import { getArrWithoutSpace } from "./../../helpers"
import { IOperator } from "../../models"
import { addOperatorId } from "./../../store/actions"
import { TTypes } from "./../../store/types"

export const Header: React.FC = () => {
  const [operators, setOperators] = useState<IOperator[]>([])
  const [selectedItem, setSelectedItem] = useState<IOperator | null>(null)

  const dispatch: Dispatch<TTypes> = useDispatch()

  const { setErrorOperators } = useContext(Context)

  const changeOperator = (id: string): void => {
    setSelectedItem(operators?.find((item) => item.id === id) ?? null)
  }

  useEffect(() => {
    const saveOperators = async (): Promise<void> => {
      const data = await getOperators(getOperatorsUrl)
      // Чтобы убрать id в стор
      dispatch(addOperatorId(selectedItem?.id))

      // Проверка пришла ли ошибка от сервера
      if (data.error == null) {
        setOperators(getArrWithoutSpace(data.operators))
        setErrorOperators("")
      } else {
        setErrorOperators("Ошибка при получении операторов с сервера")
        console.error(data.error)
      }
    }

    saveOperators()
  }, [selectedItem])

  return (
    <header className="container-fluid d-flex justify-content-center border-bottom shadow-sm px-3">
      <div className="container d-flex justify-content-between align-items-center p-3 px-0 wrapper-header">
        <div className="d-flex align-items-center position-relative">
          <div className="header-logo px-0">
            <i className="fa-regular fa-user header-logo_icon" />
          </div>

          <div className="select-wrapper px-2">
            <SearchField
              items={operators}
              isLoading={operators?.length ? false : true}
              onChange={changeOperator}
              defaultItem={selectedItem?.id}
            />
          </div>
        </div>
        <form
          action="#"
          //method="POST"
          className="d-flex justify-content-center"
        >
          <button
            type="submit"
            className="btn border-0 px-0"
            data-mdb-toggle="tooltip"
            data-mdb-placement="bottom"
            title="Выход"
          >
            <i className="fa-solid fa-arrow-right-from-bracket form-btn_icon" />
          </button>
        </form>
      </div>
    </header>
  )
}
