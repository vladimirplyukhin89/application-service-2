import React, { useContext } from "react"

import { AddService } from "./components/AddService"
import { SuccessModal, WarningPopup } from "./../../common"
import Context from "./../../context/Context"

export const Section: React.FC = () => {
  const { errorForm, errorDate, errorAddress, errorOperators } =
    useContext(Context)

  return (
    <div className="container-fluid d-flex justify-content-center">
      <div className="d-flex flex-column align-items-center px-0 wrapper-section">
        <h1 className="fs-3 my-4 pl-3 container lh-base w-auto fw-normal section-title">
          Поверка индивидуальных приборов учёта
        </h1>
        <div className="container">
          <div className="row">
            <div className="col-12 px-0">
              <AddService />
              <SuccessModal />
              <div>
                <WarningPopup>
                  {errorOperators && <span>{errorOperators}</span>}
                  {errorDate && <span>{errorDate}</span>}
                  {errorAddress && <span>{errorAddress}</span>}
                  {errorForm && <span>{errorForm}</span>}
                </WarningPopup>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
