import React from "react"

import { IFAQ } from "../../../../models"

const faq: IFAQ[] = [
  {
    title: "Стоимость поверки одного прибора учёта",
    subtitle:
      "Стоимость услуги в Тюмени 750 рублей. В населённых пунктах с.Каменка, с.Кулаково, с.Мальково, д.Паренкина, д.Ошкукова, д.Субботина, п.Онохино, с.Червишево, д.Большие Акияры стоимость 1500 рублей.",
  },
  {
    title: "Технический паспорт прибора",
    subtitle:
      "Технический паспорт прибора обязателен. Если его нет, то прибор нужно менять и заново опломбировать.",
  },
  {
    title: "После заявки",
    subtitle:
      "Водоканал свяжется с вами. Если остались вопросы, то номер Водоканала - 88002501747",
  },
]

export const FAQ: React.FC = () => {
  return (
    <div className="container-sm border border-0 p-0 bg-light">
      <div className="container my-0">
        <h3 className="fs-3 lh-base text-center my-0 pb-0 title">F.A.Q.</h3>
      </div>

      <div className="container-sm d-flex flex-column mb-2 lists">
        <ul className="list-group list-group-light list-group-small border-0">
          {faq.map((obj, i) => {
            return (
              <li key={i} className="list-group-item pt-1 pb-1 rounded-0">
                <h3 className="text-size fw-normal mb-1">{obj.title}</h3>
                <p className="subtext-size fst-italic mb-0">{obj.subtitle}</p>
              </li>
            )
          })}
        </ul>
      </div>
    </div>
  )
}
