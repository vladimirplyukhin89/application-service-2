import React, {
  useState,
  useEffect,
  useContext,
  useRef,
  forwardRef,
} from "react"
import { useSelector } from "react-redux"
import { FormattedValues, PhoneField, Button } from "itpc-ui-kit"
import DatePicker, { registerLocale } from "react-datepicker"
import ru from "date-fns/locale/ru"

import { FAQ } from "../FAQ"
import { TextField, Address } from "../../../../common"
import InputDatepicker from "./../../../../common/InputDatepicker/InputDatepicker"
import { sendForm, getAddress, getDates } from "../../../../API"
import { postOperatorsUrl } from "../../../../constants/api"
import { formatDate, getFormattedDates } from "./../../../../helpers"
import {
  EMAIL_REGEXP,
  NUMBERS_REGEXP,
  STRING_REGEXP,
} from "../../../../constants/reg"
import Context from "../../../../context/Context"
import { IFormValues, IFormErrors } from "../../../../models"
import { IState } from "../../../../store/types"
import { useDebounce } from "./../../../../hooks"

const initialValues: IFormValues = {
  account: "",
  firstName: "",
  patronymicName: "",
  secondName: "",
  firstMobile: "",
  secondMobile: "",
  email: "",
  date: "",
  operatorId: "",
}

const initialErrors: IFormErrors = {
  accountError: "",
  firstNameError: "",
  patronymicNameError: "",
  secondNameError: "",
  firstMobileError: "",
  secondMobileError: "",
  emailError: "",
  dateError: "",
}

// Для локализации react-datepicker
registerLocale("ru", ru)

// Для костамизации раппера вокруг react-datepicker
const CustomInput = forwardRef((props: any, ref) => {
  return <InputDatepicker {...props} ref={ref} />
})

export const AddService: React.FC = () => {
  // Cтейт для отправки данных с формы на сервер
  const [values, setValues] = useState<IFormValues>(initialValues)
  // Стейт для обработки ошибок с формы
  const [errors, setErrors] = useState<IFormErrors>(initialErrors)
  // Контекст для модального окна и для попап
  const {
    setOpenModal,
    setOpenPopup,
    setErrorForm,
    setErrorDate,
    setErrorAddress,
  } = useContext(Context)
  // Блокировка кнопки
  const [disabled, setDisabled] = useState<boolean>(true)
  // Для получения адреса с сервера
  const [address, setAddress] = useState<object>({})
  // Для получения дат с сервера
  const [dates, setDates] = useState<string[]>([])
  // Для даты в react-datepicker
  const [selectedDate, setSelectedDate] = useState<Date>(null)
  // Для кастомного инпута в react-datepicker
  const inputRef: React.MutableRefObject<null> = useRef(null)
  // Для отправки ID оператора на сервер
  let operatorID: string = useSelector((state: IState) => state.id)

  // Итоговый объект с данными формы для сервера
  const data = {
    account: Number(values.account),
    last_name: values.secondName,
    first_name: values.firstName,
    patronymic_name: values.patronymicName,
    number_one: values.firstMobile,
    number_two: values.secondMobile,
    email: values.email,
    date_execution: formatDate(selectedDate),
    operator_id: Number(operatorID),
  }

  const submit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault()

    const sendData = async (): Promise<void> => {
      const dataForm = await sendForm(postOperatorsUrl, data)

      if (dataForm.error == null) {
        resetForm()
        setOpenModal(true)
        setOpenPopup(false)
        setErrorForm("")
      } else {
        setErrorForm("Ошибка при отправке формы на сервер")
        setOpenPopup(true)
        console.error(dataForm.error)
      }
    }

    sendData()
  }

  const reset = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault()
    resetForm()
  }

  function resetForm(): void {
    setValues({
      account: "",
      firstName: "",
      patronymicName: "",
      secondName: "",
      firstMobile: "",
      secondMobile: "",
      email: "",
      date: "",
      operatorId: "",
    })

    setErrors({
      accountError: "",
      firstNameError: "",
      patronymicNameError: "",
      secondNameError: "",
      firstMobileError: "",
      secondMobileError: "",
      emailError: "",
      dateError: "",
    })

    setDisabled(true)
    setOpenModal(false)
    setOpenPopup(false)
    setAddress({})
    setSelectedDate(null)
  }

  const changeAccount = (value: string): void => {
    setValues((values) => ({ ...values, account: value }))

    if (value.length && !value.match(NUMBERS_REGEXP)) {
      setErrors((errors) => ({
        ...errors,
        accountError: "Введите только числовые значения",
      }))
      return
    }

    if (value.length && value.length < 6) {
      setErrors((errors) => ({
        ...errors,
        accountError: "Длина лицевого счета должна быть от 6 до 8 символов",
      }))
      return
    }

    setErrors((errors) => ({ ...errors, accountError: "" }))
  }

  const clearAccount = (): void => {
    setValues((values) => ({ ...values, account: "" }))
    setErrors((errors) => ({ ...errors, accountError: "" }))
  }

  const changeFirstName = (value: string): void => {
    setValues((values) => ({ ...values, firstName: value }))

    if (value.length && !value.match(STRING_REGEXP)) {
      setErrors((errors) => ({
        ...errors,
        firstNameError: "Введите только буквенные значения",
      }))
      return
    }

    setErrors((errors) => ({ ...errors, firstNameError: "" }))
  }

  const clearFirstName = (): void => {
    setValues((values) => ({ ...values, firstName: "" }))
    setErrors((errors) => ({ ...errors, firstNameError: "" }))
  }

  const changepatronymicName = (value: string): void => {
    setValues((values) => ({ ...values, patronymicName: value }))

    if (value.length && !value.match(STRING_REGEXP)) {
      setErrors((errors) => ({
        ...errors,
        patronymicNameError: "Введите только буквенные значения",
      }))
      return
    }

    setErrors((errors) => ({ ...errors, patronymicNameError: "" }))
  }

  const clearpatronymicName = (): void => {
    setValues((values) => ({ ...values, patronymicName: "" }))
    setErrors((errors) => ({ ...errors, patronymicNameError: "" }))
  }

  const changeSecondName = (value: string): void => {
    setValues((values) => ({ ...values, secondName: value }))

    if (value.length && !value.match(STRING_REGEXP)) {
      setErrors((errors) => ({
        ...errors,
        secondNameError: "Введите только буквенные значения",
      }))
      return
    }

    setErrors((errors) => ({ ...errors, secondNameError: "" }))
  }

  const clearSecondName = (): void => {
    setValues((values) => ({ ...values, secondName: "" }))
    setErrors((errors) => ({ ...errors, secondNameError: "" }))
  }

  const changeFirstMobile = (values: FormattedValues): void => {
    setValues((value) => ({ ...value, firstMobile: values.value }))

    if (values.value.length && values.value.length < 10) {
      setErrors((errors) => ({
        ...errors,
        firstMobileError: "Введите номер полностью",
      }))
      return
    }

    setErrors((errors) => ({ ...errors, firstMobileError: "" }))
  }

  const changeSecondMobile = (values: FormattedValues): void => {
    setValues((value) => ({ ...value, secondMobile: values.value }))

    if (values.value.length && values.value.length < 10) {
      setErrors((errors) => ({
        ...errors,
        secondMobileError: "Введите номер полностью",
      }))
      return
    }

    setErrors((errors) => ({ ...errors, secondMobileError: "" }))
  }

  // Отправка запроса для получение доступных дат по лицевому счёту
  const debouncedSearch = useDebounce(values.account, 2500)
  let datesURL = `/v1/dates/${values.account}/`
  let addressURL = `/v1/address/${values.account}/`

  useEffect(() => {
    if (debouncedSearch) {
      const saveDates = async (): Promise<void> => {
        const data = await getDates(datesURL)
        if (data.error == null) {
          setDates(getFormattedDates(data.dates))
          setErrorDate("")
        } else {
          setErrorDate("Ошибка при получении доступных дат по лицевому счёту")
          setOpenPopup(true)
          console.error(data.error)
        }
      }

      const saveAddress = async (): Promise<void> => {
        const data = await getAddress(addressURL)
        if (data.error == null) {
          setAddress(data.address.address)
          setErrorAddress("")
        } else {
          setErrorAddress("Ошибка при получении адреса по лицевому счёту")
          setOpenPopup(true)
          console.error(data.error)
        }
      }

      saveDates()
      saveAddress()
    }
  }, [debouncedSearch])

  useEffect(() => {
    if (dates.length > 0) {
      setSelectedDate(new Date(dates[0]))
    }
  }, [dates])

  const changeDate = (date: Date) => {
    setSelectedDate(date)
  }

  const changeEmail = (value: string): void => {
    setValues((values) => ({ ...values, email: value }))

    if (!value.length || EMAIL_REGEXP.test(value)) {
      setErrors((errors) => ({ ...errors, emailError: "" }))
      return
    }

    setErrors((errors) => ({
      ...errors,
      emailError: "Введите корректный email",
    }))
  }

  const clearEmail = (): void => {
    setValues((values) => ({ ...values, email: "" }))
    setErrors((errors) => ({ ...errors, emailError: "" }))
  }

  const disableBtn = (): void => {
    if (
      values.account.length > 5 &&
      values.firstName.length &&
      values.secondName.length &&
      values.firstMobile.length > 9 &&
      selectedDate != null &&
      operatorID != null
    ) {
      setDisabled(false)
      return
    }

    setDisabled(true)
  }

  useEffect(() => {
    disableBtn()
  }, [
    values.account,
    values.firstName,
    values.secondName,
    values.firstMobile,
    selectedDate,
    operatorID,
  ])

  // Для очистки адреса по лс при изменении его в поле ввода
  useEffect(() => {
    setAddress({})
    setDates([])
    setSelectedDate(null)
  }, [debouncedSearch])

  return (
    <form onSubmit={submit} onReset={reset}>
      <div className="container d-flex flex-row-reverse border bg-light">
        <div className="row flex-row-reverse">
          <div className="col-lg-7 px-0">
            <div className="mb-3 container">
              <div className="row">
                <h2 className="fs-3 text-center title">Оставить заявку</h2>
                <div className="col-sm ">
                  <TextField
                    id="form-account"
                    name="form-account"
                    placeholder="Лицевой счёт"
                    value={values.account.trimStart()}
                    onChange={changeAccount}
                    isShowButton={values.account.length > 0}
                    onClickButton={clearAccount}
                    buttonChild={<i className="fa-solid fa-xmark" />}
                    maxLength={8}
                    errorMessage={errors.accountError}
                    validationState={
                      errors.accountError.length ? "invalid" : "valid"
                    }
                  />
                  {Object.keys(address).length != 0 ? (
                    <Address address={address} />
                  ) : null}
                </div>
              </div>
            </div>

            <div className="mb-3 container">
              <div className="row">
                <div className="col-sm">
                  <TextField
                    id="form-secondName"
                    name="form-secondName"
                    placeholder="Фамилия"
                    value={values.secondName.trimStart()}
                    onChange={changeSecondName}
                    isShowButton={values.secondName.length > 0}
                    onClickButton={clearSecondName}
                    buttonChild={<i className="fa-solid fa-xmark" />}
                    errorMessage={errors.secondNameError}
                    validationState={
                      errors.secondNameError.length ? "invalid" : "valid"
                    }
                  />
                </div>
              </div>
            </div>

            <div className="mb-3 container">
              <div className="row">
                <div className="col-sm right-space">
                  <TextField
                    id="form-firstName"
                    name="form-firstName"
                    placeholder="Имя"
                    value={values.firstName.trimStart()}
                    onChange={changeFirstName}
                    isShowButton={values.firstName.length > 0}
                    onClickButton={clearFirstName}
                    buttonChild={<i className="fa-solid fa-xmark" />}
                    errorMessage={errors.firstNameError}
                    validationState={
                      errors.firstNameError.length ? "invalid" : "valid"
                    }
                  />
                </div>
                <div className="col-sm">
                  <TextField
                    id="form-patronymicName"
                    name="form-patronymicName"
                    placeholder="Отчество (необязательно)"
                    value={values.patronymicName.trimStart()}
                    onChange={changepatronymicName}
                    isShowButton={values.patronymicName.length > 0}
                    onClickButton={clearpatronymicName}
                    buttonChild={<i className="fa-solid fa-xmark" />}
                    errorMessage={errors.patronymicNameError}
                    validationState={
                      errors.patronymicNameError.length ? "invalid" : "valid"
                    }
                  />
                </div>
              </div>
            </div>

            <div className="mb-3 container">
              <div className="row">
                <div className="col-sm right-space">
                  <PhoneField
                    id="form-first-mobile"
                    name="form-first-mobile"
                    placeholder="Номер телефона"
                    value={values.firstMobile}
                    onChange={changeFirstMobile}
                    errorMessage={errors.firstMobileError}
                    validationState={
                      errors.firstMobileError.length ? "invalid" : "valid"
                    }
                  />
                </div>
                <div className="col-sm">
                  <PhoneField
                    id="form-second-mobile"
                    name="form-second-mobile"
                    placeholder="Запасной номер (необязательно)"
                    value={values.secondMobile}
                    onChange={changeSecondMobile}
                    errorMessage={errors.secondMobileError}
                    validationState={
                      errors.secondMobileError.length ? "invalid" : "valid"
                    }
                  />
                </div>
              </div>
            </div>
            <div className="mb-3 container">
              <div className="row">
                <div className="col-sm right-space">
                  <DatePicker
                    selected={selectedDate}
                    onChange={changeDate}
                    locale={ru}
                    dateFormat="dd.MM.yyyy"
                    includeDates={dates.map((date) => new Date(date))}
                    customInput={<CustomInput inputRef={inputRef} />}
                  />
                </div>
                <div className="col-sm">
                  <TextField
                    id="form-email"
                    name="form-email"
                    placeholder="Email (необязательно)"
                    value={values.email}
                    onChange={changeEmail}
                    isShowButton={values.email.length > 0}
                    onClickButton={clearEmail}
                    buttonChild={<i className="fa-solid fa-xmark" />}
                    errorMessage={errors.emailError}
                    validationState={
                      errors.emailError.length ? "invalid" : "valid"
                    }
                  />
                </div>
              </div>
            </div>

            <div className="container px-4 d-flex mb-2 justify-content-end">
              <div className="btns">
                <Button type="reset" variant="red">
                  <span className="btns-first_left"> </span>
                  Сбросить
                  <span className="btns-first_right"> </span>
                </Button>
              </div>
              <div className="btns">
                <Button type="submit" variant="purple" disabled={disabled}>
                  <span className="btns-second_left"> </span>
                  Отправить
                  <span className="btns-second_right"> </span>
                </Button>
              </div>
            </div>
          </div>
          <div className="col-lg-5 px-0">
            <FAQ />
          </div>
        </div>
      </div>
    </form>
  )
}
