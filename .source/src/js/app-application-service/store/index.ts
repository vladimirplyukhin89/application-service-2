import { createStore, Store } from "redux"

import { rootReducer } from "./reducers"
import { IState, TTypes } from "./types"

const store: Store<IState, TTypes> = createStore(rootReducer)

export default store
