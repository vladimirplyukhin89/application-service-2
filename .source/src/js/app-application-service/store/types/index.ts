import { ADD_OPERATOR_ID, ADD_OPERATOR_NAME } from "./../actions"

export type TTypes = {
  type: typeof ADD_OPERATOR_ID | typeof ADD_OPERATOR_NAME
  payload?: string
}

export interface IState {
  id: string
  username: string
  payload?: string[]
}
