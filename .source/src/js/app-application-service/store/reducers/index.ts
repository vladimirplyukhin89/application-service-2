import { ADD_OPERATOR_ID, ADD_OPERATOR_NAME } from "./../actions"
import { IState, TTypes } from "./../types"

const initialState: IState = { id: "", username: "" }

export const rootReducer = (state = initialState, action: TTypes): IState => {
  switch (action.type) {
    case ADD_OPERATOR_ID:
      return { ...state, id: action.payload }
    case ADD_OPERATOR_NAME:
      return { ...state, username: action.payload }
    default:
      return state
  }
}
