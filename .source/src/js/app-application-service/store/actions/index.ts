import { TTypes } from "../types"

export const ADD_OPERATOR_ID: string = "ADD_OPERATOR_ID"
export const ADD_OPERATOR_NAME: string = "ADD_OPERATOR_NAME"

export const addOperatorId = (id: string): TTypes => ({
  type: ADD_OPERATOR_ID,
  payload: id,
})

export const addOperatorName = (username: string): TTypes => ({
  type: ADD_OPERATOR_NAME,
  payload: username,
})
