// Для форматирования итоговой даты в тело запроса на бек
export function formatDate(date: Date): string {
  return String(
    new Date(date).toLocaleDateString().split(".").reverse().join("-")
  )
}
