import { IOperator } from "./../models"

// Для фильтрации лишних пробелов в ФИО оператора
export function getArrWithoutSpace(arr: IOperator[]): IOperator[] {
  const temp: IOperator[] = []

  for (const el of arr) {
    temp.push({
      ...el,
      username: el.username.replace(/\s+/g, " ").trim(),
    })
  }

  return temp
}
