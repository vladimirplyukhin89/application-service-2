// Для обработки доступных дат с бека для Datepicker
export function getFormattedDates(arr: string[]): string[] {
  return arr.map((date) => new Date(date).toISOString())
}
