import { IFormValues, IError } from "./../models"

interface Response {
  result?: IFormValues
  error?: IError
}

export const sendForm = async (
  url: string,
  data: object
): Promise<Response> => {
  try {
    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
      headers: { "Content-Type": "application/json" },
    })
    if (response.status >= 400) {
      throw new Error(
        `Ошибка ${response.status} при отправке данных формы на сервер`
      )
    }
    const result = await response?.json()
    return {
      result,
    }
  } catch (e) {
    const error = e
    console.error(error)
    return {
      error,
    }
  }
}
