import { IOperator, IError } from "./../models"

interface Response {
  operators?: IOperator[]
  error?: IError
}

export const getOperators = async (url: string): Promise<Response> => {
  try {
    const response = await fetch(url, {
      method: "GET",
    })
    if (response.status >= 400) {
      throw new Error(`Ошибка ${response.status} при запросе списка операторов`)
    }
    const operators = await response?.json()
    return {
      operators,
    }
  } catch (e) {
    const error = e
    console.error(error)
    return {
      error,
    }
  }
}
