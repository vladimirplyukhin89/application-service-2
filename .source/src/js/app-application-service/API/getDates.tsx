import { IError } from "../models"

interface Response {
  dates?: string[]
  error?: IError
}

export const getDates = async (url: string): Promise<Response> => {
  try {
    const response = await fetch(url, {
      method: "GET",
    })
    if (response.status >= 400) {
      throw new Error(
        `Ошибка ${response.status} при запросе допустимых дат поверки по данному лицевому счёту`
      )
    }
    const dates = await response?.json()
    return {
      dates,
    }
  } catch (e) {
    const error = e
    console.error(error)
    return {
      error,
    }
  }
}
