import { IAddress, IError } from "./../models"

interface Response {
  address?: IAddress
  error?: IError
}

export const getAddress = async (url: string): Promise<Response> => {
  try {
    const response = await fetch(url, {
      method: "GET",
    })
    if (response.status >= 400) {
      throw new Error(
        `Ошибка ${response.status} при запросе адреса по данному лицевому счёту`
      )
    }
    const address = await response?.json()
    return {
      address,
    }
  } catch (e) {
    const error = e
    console.error(error)
    return {
      error,
    }
  }
}
