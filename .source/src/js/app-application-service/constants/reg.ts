export const EMAIL_REGEXP = /^[a-zA-Z0-9-\._]+@([-a-z0-9]+\.)+[a-z]{2,4}$/
export const NUMBERS_REGEXP = /^\d+$/
export const STRING_REGEXP = /^[А-яЁё a-zA-Z]+$/
