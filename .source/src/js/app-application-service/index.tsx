import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"

import { Header } from "./components/User"
import { Section } from "./components/Section"
import { ContextProvider } from "./context/Context"
import store from "./store"

if (document.getElementById("user")) {
  ReactDOM.render(
    <div>
      <Provider store={store}>
        <ContextProvider>
          <Header />
        </ContextProvider>
      </Provider>
    </div>,
    document.getElementById("user")
  )
}

if (document.getElementById("section")) {
  ReactDOM.render(
    <div>
      <Provider store={store}>
        <ContextProvider>
          <Section />
        </ContextProvider>
      </Provider>
    </div>,
    document.getElementById("section")
  )
}
